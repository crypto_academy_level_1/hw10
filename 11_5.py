import math

def abs(x):

    return x if x >= 0 else -x

def t(n,epsilon):

    return 2**((n+1)/2) * math.sqrt(abs(math.log(1/(1-epsilon))))

def exponent(n,epsilon):

    return (math.log(t(n,epsilon))/math.log(2))

'''
print("n: 80 bits and epsilon: 0.5: " + str(exponent(80,0.5)))

print("n: 128 bits and epsilon: 0.5: " + str(exponent(128,0.5)))

print("n: 128 bits and epsilon: 0.9: " + str(exponent(128,0.9)))

print("n: 160 bits and epsilon: 0.5: " + str(exponent(160,0.5)))

print("n: 160 bits and epsilon: 0.9: " + str(exponent(160,0.9)))

print("n: 256 bits and epsilon: 0.5: " + str(exponent(256,0.5)))

print("n: 256 bits and epsilon: 0.9: " + str(exponent(256,0.9)))

print("n: 384 bits and epsilon: 0.5: " + str(exponent(384,0.5)))

print("n: 384 bits and epsilon: 0.9: " + str(exponent(384,0.9)))

print("n: 512 bits and epsilon: 0.5: " + str(exponent(512,0.5)))

print("n: 512 bits and epsilon: 0.9: " + str(exponent(512,0.9)))
'''

print("n: 64 bits and epsilon: 0.5: " + str(t(64,0.5)))

print("n: 64 bits and epsilon: 0.9: " + str(t(64,0.9)))

print("n: 128 bits and epsilon: 0.5: " + str(t(128,0.5)))

print("n: 128 bits and epsilon: 0.9: " + str(t(128,0.9)))

print("n: 256 bits and epsilon: 0.5: " + str(t(256,0.5)))

print("n: 256 bits and epsilon: 0.9: " + str(t(256,0.9)))
